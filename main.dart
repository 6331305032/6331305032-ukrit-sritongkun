import 'package:flutter/material.dart';

void main() {
  var app = runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "MFU-Wallet",
      home: Homepage(),
      theme: ThemeData(primarySwatch: Colors.lightBlue),
    );
  }
}

class Homepage extends StatefulWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Icon(Icons.arrow_back),
              Text(
                "Transfer",
                style: TextStyle(fontSize: 25),
              ),
              Icon(Icons.supervised_user_circle_sharp)
            ],
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            text1(),
            text2(),
            text3(),
            text4(),
            sendnowButton(),
          ],
        ),
      ),
    );
  }
}

Widget sendnowButton() {
  return RaisedButton(
    color: Colors.blue,
    child: Text(
      'Send Now',
      style: TextStyle(color: Colors.white),
    ),
    onPressed: () {},
  );
}

Widget ShowButtonsSendnow() {
  return Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        sendnowButton(),
      ],
    ),
  );
}

Widget text1() {
  return Container(
    padding: EdgeInsets.all(20),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text(
        "Select Digital Assets\n\nBlackBit\n________________________________________________________________________",
      ),
    ),
  );
}

Widget text2() {
  return Container(
    padding: EdgeInsets.all(20),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text(
        "How Much\n\n205.00\n________________________________________________________________________",
      ),
    ),
  );
}

Widget text3() {
  return Container(
    padding: EdgeInsets.all(20),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text(
        "Reciver Name\n\nFahim Ahmed\n________________________________________________________________________",
      ),
    ),
  );
}

Widget text4() {
  return Container(
    padding: EdgeInsets.all(20),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text(
        "Reciver Account Number\n\nFahim Ahmed\n________________________________________________________________________",
      ),
    ),
  );
}
